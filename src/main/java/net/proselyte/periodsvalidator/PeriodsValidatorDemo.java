package net.proselyte.periodsvalidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.System.*;

public class PeriodsValidatorDemo {
    public static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));

    public static void main(String[] args) {
        out.println("Enter data: ");

        String line = readString();

        if (line != null) {
            PeriodsValidator periodsValidation = new PeriodsValidator();
            out.println(periodsValidation.findIntersections(line));
        }
    }

    public static String readString() {
        String line = null;
        try {
            line = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }
}
